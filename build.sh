#!/bin/bash
# Setup This Repo
echo "Setup Repo";
FILE=./config/settings.py
if ! [ -f "$FILE" ]; then
    cp ./config/settings.sample.py $FILE;
fi
DIR=./logs
if ! [ -d "$DIR" ]; then
    mkdir logs;
fi
git clone https://gitlab.com/MathiusD/waktisanat-db;
make -C waktisanat-db;
cp -r waktisanat-db/models .;
cp -r waktisanat-db/own .;
cp -r waktisanat-db/data .;
cp -r waktisanat-db/models_utils .;
cp -r waktisanat-db/cdnwakfu .;
cp -r waktisanat-db/building .;
cp -r waktisanat-db/config_wakdata .;
cp waktisanat-db/fetch.sh ./fetch.sh;
cp waktisanat-db/constants.py ./constants.py;
cp waktisanat-db/fetch.py ./fetch.py;
cp waktisanat-db/config/settings_wakdata.py ./config/settings_wakdata.py;
rm -rf waktisanat-db;
git clone https://gitlab.com/MathiusD/owninstall;
cp owninstall/install.py own_install.py;
python3 build_here.py;
rm -rf owninstall;